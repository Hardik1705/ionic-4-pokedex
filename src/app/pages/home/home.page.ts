import {Component, OnInit, ViewChild} from '@angular/core';
import {PokemonService} from '../../services/pokemon.service';
import {IonInfiniteScroll} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    // @ts-ignore
    @ViewChild(IonInfiniteScroll) infinite: IonInfiniteScroll;

    private offset = 0;
    private pokemon: any =[];

    constructor(
        private pokeService: PokemonService
    ) {
    }

    loadPokemon(loadMore = false, event?) {
        if (loadMore) {
            this.offset += 25;
        }

        this.pokeService.getPokemon(this.offset).subscribe(res => {
            this.pokemon = [...this.pokemon, ...res];

            if (event) {
                event.target.complete();
            }

            if (this.offset === 125) {
                this.infinite.disabled = true;
            }
        });
    }

    onSearchChange(e) {
        console.log('eeeeee', e);
        const value = e.detail.value;

        if (value === '') {
            this.offset = 0;
            this.loadPokemon();
            return;
        }

        this.pokeService.findPokemon(value).subscribe(res => {
            this.pokemon = [res];
        }, err => {
            this.pokemon = [];
        });
    }

    ngOnInit() {
        this.loadPokemon();
    }

}
